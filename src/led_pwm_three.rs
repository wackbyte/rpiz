//! Blink red, green, and blue LEDs with software-based PWM.

use rppal::gpio::Gpio;
use std::{error::Error, thread::sleep, time::Duration};

const INTERVAL: Duration = Duration::from_micros(100);
const PERIOD: Duration = Duration::from_millis(20);
const PULSE_MIN: u64 = 0;
const PULSE_MAX: u64 = 20_000;
const GPIO_RED_LED: u8 = 17;
const GPIO_GREEN_LED: u8 = 27;
const GPIO_BLUE_LED: u8 = 22;

fn main() -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;

    let mut red = gpio.get(GPIO_RED_LED)?.into_output();
    let mut green = gpio.get(GPIO_GREEN_LED)?.into_output();
    let mut blue = gpio.get(GPIO_BLUE_LED)?.into_output();

    for pulse in (PULSE_MIN..=PULSE_MAX).rev() {
        let pulse = Duration::from_micros(pulse);
        red.set_pwm(PERIOD, pulse)?;
        green.set_pwm(PERIOD, pulse)?;
        blue.set_pwm(PERIOD, pulse)?;
        sleep(INTERVAL);
    }

    Ok(())
}
