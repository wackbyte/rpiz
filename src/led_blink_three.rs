//! Blink red, green, and blue LEDs.

use rppal::gpio::Gpio;
use std::{error::Error, thread::sleep, time::Duration};

const INTERVAL: Duration = Duration::from_millis(100);
const GPIO_RED_LED: u8 = 17;
const GPIO_GREEN_LED: u8 = 27;
const GPIO_BLUE_LED: u8 = 22;

fn main() -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;

    let mut red = gpio.get(GPIO_RED_LED)?.into_output();
    let mut green = gpio.get(GPIO_GREEN_LED)?.into_output();
    let mut blue = gpio.get(GPIO_BLUE_LED)?.into_output();

    for _ in 0..100 {
        red.set_high();
        sleep(INTERVAL);
        green.set_high();
        sleep(INTERVAL);
        blue.set_high();
        sleep(INTERVAL);
        red.set_low();
        sleep(INTERVAL);
        green.set_low();
        sleep(INTERVAL);
        blue.set_low();
        sleep(INTERVAL);
    }

    Ok(())
}
