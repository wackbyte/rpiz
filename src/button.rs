//! Get input from a button.

use rppal::gpio::Gpio;
use std::error::Error;

const GPIO_BUTTON: u8 = 17;

fn main() -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;

    let button = gpio.get(GPIO_BUTTON)?.into_input_pulldown();

    loop {
        if button.is_high() {
            println!("Button is being pressed!");
        }
    }
}
