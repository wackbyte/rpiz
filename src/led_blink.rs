//! Blink an LED.

use rppal::gpio::Gpio;
use std::{error::Error, thread::sleep, time::Duration};

const INTERVAL: Duration = Duration::from_millis(50);
const GPIO_LED: u8 = 17;

fn main() -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;

    let mut led = gpio.get(GPIO_LED)?.into_output();

    for _ in 0..100 {
        led.set_high();
        sleep(INTERVAL);
        led.set_low();
        sleep(INTERVAL);
    }

    Ok(())
}
