//! Blink an LED with software-based PWM.

use rppal::gpio::Gpio;
use std::{error::Error, thread::sleep, time::Duration};

const INTERVAL: Duration = Duration::from_millis(5);
const PERIOD: Duration = Duration::from_millis(20);
const PULSE_MIN: u64 = 0;
const PULSE_MAX: u64 = 5000;
const GPIO_LED: u8 = 17;

fn main() -> Result<(), Box<dyn Error>> {
    let gpio = Gpio::new()?;

    let mut led = gpio.get(GPIO_LED)?.into_output();

    for pulse in PULSE_MIN..=PULSE_MAX {
        led.set_pwm(PERIOD, Duration::from_micros(pulse))?;
        sleep(INTERVAL);
    }

    Ok(())
}
